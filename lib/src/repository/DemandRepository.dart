import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:structurepublic/src/models/DemandData.dart';
import 'package:structurepublic/src/models/WorkerData.dart';
import 'package:structurepublic/src/models/user.dart';



import '../helpers/custom_trace.dart';
import '../models/setting.dart';

ValueNotifier<Setting> setting = new ValueNotifier(new Setting());
final navigatorKey = GlobalKey<NavigatorState>();


Future<Userss> getUserDemand(DemandData demandData) async {
  print("hello000000000000000000000000000000") ;
  print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz1"+demandData.id);
  Userss userDemand=new Userss();

  await FirebaseFirestore.instance
      .collection("users")
      .doc(demandData.idUser)
      .get()
      .then((value) {
    print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz2");
    print("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm1" +Userss.fromJson(value.data()).image.toString());
    userDemand= Userss.fromJson(value.data()) ;
  }).catchError((e) {print("ffffffffffffooooooooooooooooooooffffffffffffffffooooooooooo");});
  print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz3");
//  print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz4"+userDemand.image.toString()+userDemand.name+userDemand.phone.toString());
  return userDemand;
}

Future<List<DemandData>> getDemandNode(WorkerData workerData) async {
  var url = Uri.parse('https://infinityserver2020.herokuapp.com/demand/get/worker/');

  http.Response res = await http.post(
    url,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body:jsonEncode({"id_worker":workerData.id}),
  );
  if (res.statusCode == 200) {
    List<dynamic> body = jsonDecode(res.body);
    List<DemandData> demand=[];
    demand=body.map((p) => DemandData.fromJson(p)).toList();

    print(demand.toString()+"mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm2");
    return demand;
  } else {
    List<DemandData> demand=[];
    return demand;
  }
}

void postDamandNode(DemandData demandData)async{
  var url = Uri.parse('https://infinityserver2020.herokuapp.com/demand/admin/post/');
  var response = await http.post(
    url,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({
      'done':demandData.done,
      'product' : demandData.product.map((v) => v.toJson()).toList(),
      'id': demandData.id,
      'id_user':demandData.idUser,
      'id_worker':demandData.idWorker,
      'id_market': demandData.idMarket,
      'id_admins':demandData.idAdmins,
      'state_ar':demandData.stateAr,
      'state_en': demandData.stateEn,
      'remove': demandData.remove,
      'timesTampdemand': demandData.timesTampdemand,
      'timesTampCreat': demandData.timesTampCreat,
      'priceTotal':demandData.priceTotal,
      'rating':demandData.rating,
      'iscoupon':demandData.iscoupon,
      'notification':false
    }),
  );
  print(response.body.toString());
}

