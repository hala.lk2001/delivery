import 'dart:async';

import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:mvc_pattern/mvc_pattern.dart';



import 'package:structurepublic/src/models/DemandData.dart';
import 'package:structurepublic/src/models/WorkerData.dart';
import 'package:structurepublic/src/models/user.dart';

import 'package:structurepublic/src/repository/DemandRepository.dart' as repo;
import 'package:url_launcher/url_launcher.dart';

import '../../generated/l10n.dart';
import '../helpers/helper.dart';

import 'package:firebase_auth/firebase_auth.dart';

class DemandController extends ControllerMVC {
  bool loading = false;
  GlobalKey<ScaffoldState> scaffoldKey;
  List<DemandData> listByWorker = [];
  final WorkerData worker;
  List<Userss> selectUser = [];
int time ;
  OverlayEntry loader;

  DemandController(this.worker) {
    loader = Helper.overlayLoader(context);
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  @override
  void initState() {
    super.initState();
    getDemands();
  }

  // Future<Userss> getDemandUser(DemandData demandData)async{
  //   Userss userss=new Userss();
  //   await repo.getUserDemand(demandData).then((value){
  //     userss= value;
  //   });
  //   print("shahedddddddddddddddddddddddd5"+userss.name+userss.phone.toString()+userss.image);
  //   return userss;
  // }

 /* void selectTimeOpen() async {
    final TimeOfDay newTime = await showTimePicker(
      context: context,
      initialTime: open,
    );
    if (newTime != null) {
      setState(() {
        open = newTime;
      });
    }

    print('lllllllllllllllaaaaaaaaaaaaaaaaarrrrrrrrrrrgggggggggggg\t\t\t'+DateTime.utc(open.hour,open.minute).toString());
  }*/
  void getData(DemandData demanddata)async{

    DatePicker.showDateTimePicker(context,
        showTitleActions: true,
        minTime: DateTime.now(),
        maxTime: DateTime.now().add(Duration(days: 45)),
        theme:DatePickerTheme( headerColor: Colors.orange, backgroundColor: Colors.blue, itemStyle: TextStyle( color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18), doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
        onChanged: (date) {
          print('change $date in timezone ' + date.timeZoneName.toString());
          time=date.microsecondsSinceEpoch ;
          },
              onConfirm: (date)
          {
            print('confirm $date');
            time=date.microsecondsSinceEpoch ;
          }, locale:LocaleType.en

    ) ;
       // child: Text('show date time picker(Chinese)', style: TextStyle(color: Colors.blue),)),


  }


  /*void getData(DemandData demanddata)async{
    DatePicker.showDatePicker(context,
        showTitleActions: true, minTime: DateTime(demanddata.timesTampCreat), maxTime: DateTime(demanddata.timesTampCreat).add(Duration(days: 45)), theme:
        DatePickerTheme( headerColor: Colors.orange,
            backgroundColor: Colors.blue, itemStyle:
            TextStyle( color: Colors.white, fontWeight:
            FontWeight.bold, fontSize: 18), doneStyle:
            TextStyle(color: Colors.white, fontSize: 16)),
        onChanged: (date) { print('change $date in timezone ' + date.timeZoneOffset.inHours.toString());
        time=date.microsecondsSinceEpoch;
        },
        onConfirm: (date) { print('confirm $date');
        time=date.microsecondsSinceEpoch ;
        },
        currentTime: DateTime.now(), locale:
        LocaleType.en);
    //  child: Text( 'show datepicker(custom theme &date time range)', style: TextStyle(color: Colors.blue), )),

    // demandController.geticon(demandData) ;



  }*/
  void getDemands() async {
    print("getdemand") ;
    setState(() {
      listByWorker.clear();

      selectUser.clear();
    });

    await repo.getDemandNode(this.worker).then((value) {
      print("getdemandnodeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee") ;

      value.forEach((element) async {

        await repo.getUserDemand(element).then((elementUser) {
          print("userrrrrrrrrrrrrrrrrrrrrrrrrr") ;
//          print("999999999999999999" + elementUser.name);
          setState(() {
            selectUser.add(elementUser);
            listByWorker.add(element);
          });
        });
      });
    });
  }

  void showLocation(Userss userss) async {
    await showLocationPicker(
      context,
      "AIzaSyDO3WfoiEpkRKCiMePjGszgKZdHvycs_jI",
      initialCenter: LatLng(userss.lat.toDouble(), userss.long.toDouble()),
      automaticallyAnimateToCurrentLocation: false,
      myLocationButtonEnabled: true,
      requiredGPS: true,
      layersButtonEnabled: true,
    );
  }

  void launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }





  void getByMarket() {}
  void acceptdemand(DemandData demandata)async{
    if(demandata.stateEn=="accepted ..."&&demandata.stateAr=="مقبول ...") {
      demandata.stateEn="Delivering ..." ;
      demandata.stateAr="جاري توصيل ..." ;
    }
   else if(demandata.stateEn=="Delivering ..."&&demandata.stateAr=="جاري توصيل ...") {

      demandata.stateEn="Delivered ..." ;
      demandata.stateAr="تم توصيل ..." ;
    }

    await repo.postDamandNode(demandata ) ;
    setState(() {
      demandata ;
      listByWorker ;
    });
  }
}










