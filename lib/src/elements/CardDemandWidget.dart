import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:structurepublic/src/controler/DemandController.dart';

import 'package:structurepublic/src/models/DemandData.dart';
import 'package:structurepublic/src/models/WorkerData.dart';
import 'package:structurepublic/src/models/user.dart';


import 'package:structurepublic/src/pages/InfoDemandPage.dart';


class CardDemandWidget extends StatefulWidget {
  const CardDemandWidget(this.demandData, this.user, this.demandController);

  final DemandData demandData;
  final Userss user;
  final DemandController demandController;

  @override
  _CardDemanWidgetState createState() {
    return _CardDemanWidgetState(
        this.demandData, this.user, this.demandController);
  }
}

class _CardDemanWidgetState extends State<CardDemandWidget> {
  _CardDemanWidgetState(this.demandData, this.user, this.demandController);

  final DemandData demandData;
  final Userss user;
  final DemandController demandController;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: double.maxFinite,
      child: Card(
        semanticContainer: true,
        color: demandData.stateAr=="تم توصيل ..."?Colors.grey:Colors.white,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: GestureDetector(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                   CircleAvatar(
                      radius: 30,
                      backgroundColor: Colors.black26,
                      backgroundImage: CachedNetworkImageProvider(
                        user.image,
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                         user.name,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Text(user.phone.toString()),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width / 10,
              ),

                SizedBox(
                  width: 100,
                ),
             // if(demandData.stateEn!="Delivered.."||demandData.stateEn!="تم توصيل..")

                Row(
               //  mainAxisAlignment: MainAxisAlignment.,
                  children: <Widget>[
                    if(((demandData.stateAr=="مقبول ...") && (demandData.stateEn=="accepted ..."))|| ((demandData.stateEn=="Delivering ...")&&(demandData.stateAr=="جاري توصيل ...")))
                   MaterialButton(
                      minWidth: 5,
                      child: Icon(demandData.stateEn=="accepted ..."&& demandData.stateAr=="مقبول ..."?Icons.shopping_cart_outlined:Icons.thumb_up_alt_outlined ,color: Colors.orangeAccent,),
                      onPressed: () {
                        setState(() async {
                          if((demandData.stateAr=="مقبول ...") && (demandData.stateEn=="accepted ..."))
                          {
                           await demandController.getData(demandData) ;
                            demandData.timesTampdemand=demandController.time ;
                          }
                          demandController.acceptdemand(demandData);


                        });
                        /* DatePicker.showDatePicker(context,
                         + showTitleActions: true, minTime: DateTime(2021,
                              1, 1), maxTime: DateTime(2023, 1, 1), theme:
                          DatePickerTheme( headerColor: Colors.orange,
                              backgroundColor: Colors.blue, itemStyle:
                              TextStyle( color: Colors.white, fontWeight:
                              FontWeight.bold, fontSize: 18), doneStyle:
                              TextStyle(color: Colors.white, fontSize: 16)),
                          onChanged: (date) { print('change $date in timezone ' + date.timeZoneOffset.inHours.toString()); },
                              onConfirm: (date) { print('confirm $date'); },
                            currentTime: DateTime.now(), locale:
                            LocaleType.en);
                    //  child: Text( 'show datepicker(custom theme &date time range)', style: TextStyle(color: Colors.blue), )),

                      // demandController.geticon(demandData) ;

    
    }*/

                      }
                    ),

                     if((demandData.stateAr=="تم توصيل ...") && (demandData.stateEn=="Delivered ..."))
                         Icon(Icons.done, color: Colors.green),

                  ],
                ),

            ],
          ),
          onDoubleTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context)=>InfoDemand((demandData), user, demandController) ,
              ),
            ) ;
          },
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context)=>InfoDemand((demandData), user, demandController) ,
              ),
            ) ;
          },
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
      ),
    );
  }
}

