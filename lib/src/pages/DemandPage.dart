

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:splashscreen/splashscreen.dart';
import 'dart:async';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart';

import 'package:structurepublic/src/controler/DemandController.dart';

import 'package:structurepublic/src/elements/CardDemandWidget.dart';
import 'package:structurepublic/src/models/WorkerData.dart';



class AdminDemand extends StatefulWidget {
  //final CategorizeData categorizeData;

  final WorkerData worker;

  @override
  AdminDemand(this.worker);

  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AdminDemand(this.worker);
  }
}

class _AdminDemand extends StateMVC<AdminDemand> {
  DemandController _con;
  final WorkerData worker;

  _AdminDemand(this.worker) : super(DemandController(worker)) {
    _con = controller;
  }

  DemandController _get() {
    return _con;
  }

  int point = 2;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("الزبائن",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
      // backgroundColor: Theme.of(context).accentColor,
          backgroundColor:Colors.orangeAccent,
       // backgroundColor: Theme.of(context).accentColor,
      ),
      body:Stack(
        children: <Widget>[

          // new Center(
          //   child: new SizedBox(
          //     height: 30.0,
          //     width: 30.0,
          //     child: new CircularProgressIndicator(
          //       value: null,
          //       strokeWidth: 7.0,
          //     ),
          //   ),
          // ),
          new ListView.builder(
            shrinkWrap: true,
            itemCount: _con.listByWorker.length,

            itemBuilder: (context, i) {

              return CardDemandWidget(
                  _con.listByWorker[i], _con.selectUser[i], _con);
            },
          ),
        ],
      ),

    );
  }
}

